﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] private int shootCount = 12;
    [SerializeField] private Color active = Color.red;
    [SerializeField] private Color inactive = Color.white;
    [SerializeField] private float freezeTime = 6f;
    [SerializeField] private Transform bulletSpawn;
    [SerializeField] private Bullet bulletPrefab;
    private SpriteRenderer renderer;

    private void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        InvokeRepeating(nameof(Shoot), freezeTime,.5f);
        
    }

    private void Shoot()
    {
        if (shootCount <= 0)
        {
            renderer.color = inactive;
            return;
        }

        shootCount--;
        renderer.color = active;
        var rotateAngle = UnityEngine.Random.Range(15f, 45f);
        transform.Rotate(0,0,rotateAngle);
        var bullet = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        bullet.Init(transform.rotation);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
    }

    public void Reset()
    {
        shootCount = 12;
    }
}


