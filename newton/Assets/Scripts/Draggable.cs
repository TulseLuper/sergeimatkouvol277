﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    private Camera camera;
    private float cameraDistance;
    
    private void Start()
    {
        camera = Camera.main;
        cameraDistance = camera.transform.position.z;
    }

    private void OnMouseDrag()
    {
        var sreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cameraDistance);
 
        var pos = camera.ScreenToWorldPoint(sreenPoint);
        transform.position = pos;
    }
}
