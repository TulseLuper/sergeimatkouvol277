﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float lifeTimeMin = .25f;
    [SerializeField] private float lifeTimeMax = 1f;
    [SerializeField] private float speed = 4;
    private Vector3 direction;
    private Transform trans;
    private float lifeTime;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var t = other.GetComponent<Tower>();
        if (t != null)
        {
            GameManager.DestroyTower(t);
            Destroy(gameObject);
        }
        
    }

    private void Update()
    {
        trans.position += direction * Time.deltaTime;
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            GameManager.SpawnTower(transform.position);
            Destroy(gameObject);
        }
    }

    public void Init(Quaternion direction)
    {
        trans = transform;
        this.direction =  direction * Vector3.right * speed;
        lifeTime = UnityEngine.Random.Range(lifeTimeMin, lifeTimeMax);
    }
}
