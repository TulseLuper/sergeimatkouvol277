using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Tower towerPrefab;
    [SerializeField] private TMP_Text towerCounter;
    private static List<Tower> towers = new List<Tower>();
    private static GameManager instance;
    
    
    public static void SpawnTower(Vector3 position)
    {
        if (towers.Count >= 100)
        {
            ResetTowers();
        }
        else
        {
            var newTower = Instantiate(instance.towerPrefab, position, Quaternion.identity);
            towers.Add(newTower);
            UpdateCounter();
        }
    }

    private static void ResetTowers()
    {
        foreach (Tower tower in towers)
        {
            tower.Reset();
        }
    }

    private void Awake()
    {
        instance = this;
        var firstTower = FindObjectOfType<Tower>();
        if (firstTower != null)
        {
            towers.Add(firstTower);
            UpdateCounter();
        }
    }

    private static void UpdateCounter()
    {
        instance.towerCounter.text = $"Towers: {towers.Count}";
    }

    public static void DestroyTower(Tower tower)
    {
        towers.Remove(tower);
        Destroy(tower.gameObject);
    }
}